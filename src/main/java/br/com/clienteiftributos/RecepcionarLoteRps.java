
package br.com.clienteiftributos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RecepcionarLoteRps complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RecepcionarLoteRps">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RecepcionarLoteRpsRequest" type="{http://endpoint.nfse.ws.webservicenfse.edza.com.br/}input" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RecepcionarLoteRps", propOrder = {
    "recepcionarLoteRpsRequest"
})
public class RecepcionarLoteRps {

    @XmlElement(name = "RecepcionarLoteRpsRequest")
    protected Input recepcionarLoteRpsRequest;

    /**
     * Gets the value of the recepcionarLoteRpsRequest property.
     * 
     * @return
     *     possible object is
     *     {@link Input }
     *     
     */
    public Input getRecepcionarLoteRpsRequest() {
        return recepcionarLoteRpsRequest;
    }

    /**
     * Sets the value of the recepcionarLoteRpsRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link Input }
     *     
     */
    public void setRecepcionarLoteRpsRequest(Input value) {
        this.recepcionarLoteRpsRequest = value;
    }

}
