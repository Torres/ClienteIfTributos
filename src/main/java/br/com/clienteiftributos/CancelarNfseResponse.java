
package br.com.clienteiftributos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CancelarNfseResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CancelarNfseResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CancelarNfseResponse" type="{http://endpoint.nfse.ws.webservicenfse.edza.com.br/}output" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CancelarNfseResponse", propOrder = {
    "cancelarNfseResponse"
})
public class CancelarNfseResponse {

    @XmlElement(name = "CancelarNfseResponse")
    protected Output cancelarNfseResponse;

    /**
     * Gets the value of the cancelarNfseResponse property.
     * 
     * @return
     *     possible object is
     *     {@link Output }
     *     
     */
    public Output getCancelarNfseResponse() {
        return cancelarNfseResponse;
    }

    /**
     * Sets the value of the cancelarNfseResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link Output }
     *     
     */
    public void setCancelarNfseResponse(Output value) {
        this.cancelarNfseResponse = value;
    }

}
