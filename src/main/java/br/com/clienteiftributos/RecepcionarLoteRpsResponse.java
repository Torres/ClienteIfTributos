
package br.com.clienteiftributos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RecepcionarLoteRpsResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RecepcionarLoteRpsResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RecepcionarLoteRpsResponse" type="{http://endpoint.nfse.ws.webservicenfse.edza.com.br/}output" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RecepcionarLoteRpsResponse", propOrder = {
    "recepcionarLoteRpsResponse"
})
public class RecepcionarLoteRpsResponse {

    @XmlElement(name = "RecepcionarLoteRpsResponse")
    protected Output recepcionarLoteRpsResponse;

    /**
     * Gets the value of the recepcionarLoteRpsResponse property.
     * 
     * @return
     *     possible object is
     *     {@link Output }
     *     
     */
    public Output getRecepcionarLoteRpsResponse() {
        return recepcionarLoteRpsResponse;
    }

    /**
     * Sets the value of the recepcionarLoteRpsResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link Output }
     *     
     */
    public void setRecepcionarLoteRpsResponse(Output value) {
        this.recepcionarLoteRpsResponse = value;
    }

}
