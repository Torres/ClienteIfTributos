
package br.com.clienteiftributos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ConsultarSituacaoLoteRpsResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ConsultarSituacaoLoteRpsResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ConsultarSituacaoLoteRpsResponse" type="{http://endpoint.nfse.ws.webservicenfse.edza.com.br/}output" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConsultarSituacaoLoteRpsResponse", propOrder = {
    "consultarSituacaoLoteRpsResponse"
})
public class ConsultarSituacaoLoteRpsResponse {

    @XmlElement(name = "ConsultarSituacaoLoteRpsResponse")
    protected Output consultarSituacaoLoteRpsResponse;

    /**
     * Gets the value of the consultarSituacaoLoteRpsResponse property.
     * 
     * @return
     *     possible object is
     *     {@link Output }
     *     
     */
    public Output getConsultarSituacaoLoteRpsResponse() {
        return consultarSituacaoLoteRpsResponse;
    }

    /**
     * Sets the value of the consultarSituacaoLoteRpsResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link Output }
     *     
     */
    public void setConsultarSituacaoLoteRpsResponse(Output value) {
        this.consultarSituacaoLoteRpsResponse = value;
    }

}
