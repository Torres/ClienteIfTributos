
package br.com.clienteiftributos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ConsultarSituacaoLoteRps complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ConsultarSituacaoLoteRps">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ConsultarSituacaoLoteRpsRequest" type="{http://endpoint.nfse.ws.webservicenfse.edza.com.br/}input" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConsultarSituacaoLoteRps", propOrder = {
    "consultarSituacaoLoteRpsRequest"
})
public class ConsultarSituacaoLoteRps {

    @XmlElement(name = "ConsultarSituacaoLoteRpsRequest")
    protected Input consultarSituacaoLoteRpsRequest;

    /**
     * Gets the value of the consultarSituacaoLoteRpsRequest property.
     * 
     * @return
     *     possible object is
     *     {@link Input }
     *     
     */
    public Input getConsultarSituacaoLoteRpsRequest() {
        return consultarSituacaoLoteRpsRequest;
    }

    /**
     * Sets the value of the consultarSituacaoLoteRpsRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link Input }
     *     
     */
    public void setConsultarSituacaoLoteRpsRequest(Input value) {
        this.consultarSituacaoLoteRpsRequest = value;
    }

}
