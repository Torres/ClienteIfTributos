
package br.com.clienteiftributos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ConsultarNfsePorRpsResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ConsultarNfsePorRpsResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ConsultarNfsePorRpsResponse" type="{http://endpoint.nfse.ws.webservicenfse.edza.com.br/}output" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConsultarNfsePorRpsResponse", propOrder = {
    "consultarNfsePorRpsResponse"
})
public class ConsultarNfsePorRpsResponse {

    @XmlElement(name = "ConsultarNfsePorRpsResponse")
    protected Output consultarNfsePorRpsResponse;

    /**
     * Gets the value of the consultarNfsePorRpsResponse property.
     * 
     * @return
     *     possible object is
     *     {@link Output }
     *     
     */
    public Output getConsultarNfsePorRpsResponse() {
        return consultarNfsePorRpsResponse;
    }

    /**
     * Sets the value of the consultarNfsePorRpsResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link Output }
     *     
     */
    public void setConsultarNfsePorRpsResponse(Output value) {
        this.consultarNfsePorRpsResponse = value;
    }

}
