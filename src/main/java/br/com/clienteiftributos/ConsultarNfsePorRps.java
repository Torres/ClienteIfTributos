
package br.com.clienteiftributos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ConsultarNfsePorRps complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ConsultarNfsePorRps">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ConsultarNfsePorRpsRequest" type="{http://endpoint.nfse.ws.webservicenfse.edza.com.br/}input" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConsultarNfsePorRps", propOrder = {
    "consultarNfsePorRpsRequest"
})
public class ConsultarNfsePorRps {

    @XmlElement(name = "ConsultarNfsePorRpsRequest")
    protected Input consultarNfsePorRpsRequest;

    /**
     * Gets the value of the consultarNfsePorRpsRequest property.
     * 
     * @return
     *     possible object is
     *     {@link Input }
     *     
     */
    public Input getConsultarNfsePorRpsRequest() {
        return consultarNfsePorRpsRequest;
    }

    /**
     * Sets the value of the consultarNfsePorRpsRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link Input }
     *     
     */
    public void setConsultarNfsePorRpsRequest(Input value) {
        this.consultarNfsePorRpsRequest = value;
    }

}
