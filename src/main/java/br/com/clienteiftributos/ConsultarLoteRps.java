
package br.com.clienteiftributos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ConsultarLoteRps complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ConsultarLoteRps">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ConsultarLoteRpsRequest" type="{http://endpoint.nfse.ws.webservicenfse.edza.com.br/}input" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConsultarLoteRps", propOrder = {
    "consultarLoteRpsRequest"
})
public class ConsultarLoteRps {

    @XmlElement(name = "ConsultarLoteRpsRequest")
    protected Input consultarLoteRpsRequest;

    /**
     * Gets the value of the consultarLoteRpsRequest property.
     * 
     * @return
     *     possible object is
     *     {@link Input }
     *     
     */
    public Input getConsultarLoteRpsRequest() {
        return consultarLoteRpsRequest;
    }

    /**
     * Sets the value of the consultarLoteRpsRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link Input }
     *     
     */
    public void setConsultarLoteRpsRequest(Input value) {
        this.consultarLoteRpsRequest = value;
    }

}
