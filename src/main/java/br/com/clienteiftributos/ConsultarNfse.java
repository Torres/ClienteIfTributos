
package br.com.clienteiftributos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ConsultarNfse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ConsultarNfse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ConsultarNfseRequest" type="{http://endpoint.nfse.ws.webservicenfse.edza.com.br/}input" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConsultarNfse", propOrder = {
    "consultarNfseRequest"
})
public class ConsultarNfse {

    @XmlElement(name = "ConsultarNfseRequest")
    protected Input consultarNfseRequest;

    /**
     * Gets the value of the consultarNfseRequest property.
     * 
     * @return
     *     possible object is
     *     {@link Input }
     *     
     */
    public Input getConsultarNfseRequest() {
        return consultarNfseRequest;
    }

    /**
     * Sets the value of the consultarNfseRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link Input }
     *     
     */
    public void setConsultarNfseRequest(Input value) {
        this.consultarNfseRequest = value;
    }

}
