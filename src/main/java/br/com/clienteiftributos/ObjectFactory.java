
package br.com.clienteiftributos;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the br.com.clienteiftributos package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CancelarNfseResponse_QNAME = new QName("http://endpoint.nfse.ws.webservicenfse.edza.com.br/", "CancelarNfseResponse");
    private final static QName _RecepcionarLoteRps_QNAME = new QName("http://endpoint.nfse.ws.webservicenfse.edza.com.br/", "RecepcionarLoteRps");
    private final static QName _ConsultarNfse_QNAME = new QName("http://endpoint.nfse.ws.webservicenfse.edza.com.br/", "ConsultarNfse");
    private final static QName _ConsultarNfsePorRps_QNAME = new QName("http://endpoint.nfse.ws.webservicenfse.edza.com.br/", "ConsultarNfsePorRps");
    private final static QName _ConsultarNfsePorRpsResponse_QNAME = new QName("http://endpoint.nfse.ws.webservicenfse.edza.com.br/", "ConsultarNfsePorRpsResponse");
    private final static QName _RecepcionarLoteRpsResponse_QNAME = new QName("http://endpoint.nfse.ws.webservicenfse.edza.com.br/", "RecepcionarLoteRpsResponse");
    private final static QName _ConsultarSituacaoLoteRpsResponse_QNAME = new QName("http://endpoint.nfse.ws.webservicenfse.edza.com.br/", "ConsultarSituacaoLoteRpsResponse");
    private final static QName _ConsultarLoteRpsResponse_QNAME = new QName("http://endpoint.nfse.ws.webservicenfse.edza.com.br/", "ConsultarLoteRpsResponse");
    private final static QName _ConsultarSituacaoLoteRps_QNAME = new QName("http://endpoint.nfse.ws.webservicenfse.edza.com.br/", "ConsultarSituacaoLoteRps");
    private final static QName _ConsultarNfseResponse_QNAME = new QName("http://endpoint.nfse.ws.webservicenfse.edza.com.br/", "ConsultarNfseResponse");
    private final static QName _CancelarNfse_QNAME = new QName("http://endpoint.nfse.ws.webservicenfse.edza.com.br/", "CancelarNfse");
    private final static QName _ConsultarLoteRps_QNAME = new QName("http://endpoint.nfse.ws.webservicenfse.edza.com.br/", "ConsultarLoteRps");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: br.com.clienteiftributos
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CancelarNfse }
     * 
     */
    public CancelarNfse createCancelarNfse() {
        return new CancelarNfse();
    }

    /**
     * Create an instance of {@link ConsultarLoteRps }
     * 
     */
    public ConsultarLoteRps createConsultarLoteRps() {
        return new ConsultarLoteRps();
    }

    /**
     * Create an instance of {@link ConsultarNfseResponse }
     * 
     */
    public ConsultarNfseResponse createConsultarNfseResponse() {
        return new ConsultarNfseResponse();
    }

    /**
     * Create an instance of {@link ConsultarSituacaoLoteRps }
     * 
     */
    public ConsultarSituacaoLoteRps createConsultarSituacaoLoteRps() {
        return new ConsultarSituacaoLoteRps();
    }

    /**
     * Create an instance of {@link ConsultarLoteRpsResponse }
     * 
     */
    public ConsultarLoteRpsResponse createConsultarLoteRpsResponse() {
        return new ConsultarLoteRpsResponse();
    }

    /**
     * Create an instance of {@link ConsultarSituacaoLoteRpsResponse }
     * 
     */
    public ConsultarSituacaoLoteRpsResponse createConsultarSituacaoLoteRpsResponse() {
        return new ConsultarSituacaoLoteRpsResponse();
    }

    /**
     * Create an instance of {@link RecepcionarLoteRpsResponse }
     * 
     */
    public RecepcionarLoteRpsResponse createRecepcionarLoteRpsResponse() {
        return new RecepcionarLoteRpsResponse();
    }

    /**
     * Create an instance of {@link ConsultarNfsePorRps }
     * 
     */
    public ConsultarNfsePorRps createConsultarNfsePorRps() {
        return new ConsultarNfsePorRps();
    }

    /**
     * Create an instance of {@link ConsultarNfsePorRpsResponse }
     * 
     */
    public ConsultarNfsePorRpsResponse createConsultarNfsePorRpsResponse() {
        return new ConsultarNfsePorRpsResponse();
    }

    /**
     * Create an instance of {@link ConsultarNfse }
     * 
     */
    public ConsultarNfse createConsultarNfse() {
        return new ConsultarNfse();
    }

    /**
     * Create an instance of {@link RecepcionarLoteRps }
     * 
     */
    public RecepcionarLoteRps createRecepcionarLoteRps() {
        return new RecepcionarLoteRps();
    }

    /**
     * Create an instance of {@link CancelarNfseResponse }
     * 
     */
    public CancelarNfseResponse createCancelarNfseResponse() {
        return new CancelarNfseResponse();
    }

    /**
     * Create an instance of {@link Output }
     * 
     */
    public Output createOutput() {
        return new Output();
    }

    /**
     * Create an instance of {@link Input }
     * 
     */
    public Input createInput() {
        return new Input();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CancelarNfseResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.nfse.ws.webservicenfse.edza.com.br/", name = "CancelarNfseResponse")
    public JAXBElement<CancelarNfseResponse> createCancelarNfseResponse(CancelarNfseResponse value) {
        return new JAXBElement<CancelarNfseResponse>(_CancelarNfseResponse_QNAME, CancelarNfseResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RecepcionarLoteRps }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.nfse.ws.webservicenfse.edza.com.br/", name = "RecepcionarLoteRps")
    public JAXBElement<RecepcionarLoteRps> createRecepcionarLoteRps(RecepcionarLoteRps value) {
        return new JAXBElement<RecepcionarLoteRps>(_RecepcionarLoteRps_QNAME, RecepcionarLoteRps.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultarNfse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.nfse.ws.webservicenfse.edza.com.br/", name = "ConsultarNfse")
    public JAXBElement<ConsultarNfse> createConsultarNfse(ConsultarNfse value) {
        return new JAXBElement<ConsultarNfse>(_ConsultarNfse_QNAME, ConsultarNfse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultarNfsePorRps }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.nfse.ws.webservicenfse.edza.com.br/", name = "ConsultarNfsePorRps")
    public JAXBElement<ConsultarNfsePorRps> createConsultarNfsePorRps(ConsultarNfsePorRps value) {
        return new JAXBElement<ConsultarNfsePorRps>(_ConsultarNfsePorRps_QNAME, ConsultarNfsePorRps.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultarNfsePorRpsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.nfse.ws.webservicenfse.edza.com.br/", name = "ConsultarNfsePorRpsResponse")
    public JAXBElement<ConsultarNfsePorRpsResponse> createConsultarNfsePorRpsResponse(ConsultarNfsePorRpsResponse value) {
        return new JAXBElement<ConsultarNfsePorRpsResponse>(_ConsultarNfsePorRpsResponse_QNAME, ConsultarNfsePorRpsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RecepcionarLoteRpsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.nfse.ws.webservicenfse.edza.com.br/", name = "RecepcionarLoteRpsResponse")
    public JAXBElement<RecepcionarLoteRpsResponse> createRecepcionarLoteRpsResponse(RecepcionarLoteRpsResponse value) {
        return new JAXBElement<RecepcionarLoteRpsResponse>(_RecepcionarLoteRpsResponse_QNAME, RecepcionarLoteRpsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultarSituacaoLoteRpsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.nfse.ws.webservicenfse.edza.com.br/", name = "ConsultarSituacaoLoteRpsResponse")
    public JAXBElement<ConsultarSituacaoLoteRpsResponse> createConsultarSituacaoLoteRpsResponse(ConsultarSituacaoLoteRpsResponse value) {
        return new JAXBElement<ConsultarSituacaoLoteRpsResponse>(_ConsultarSituacaoLoteRpsResponse_QNAME, ConsultarSituacaoLoteRpsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultarLoteRpsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.nfse.ws.webservicenfse.edza.com.br/", name = "ConsultarLoteRpsResponse")
    public JAXBElement<ConsultarLoteRpsResponse> createConsultarLoteRpsResponse(ConsultarLoteRpsResponse value) {
        return new JAXBElement<ConsultarLoteRpsResponse>(_ConsultarLoteRpsResponse_QNAME, ConsultarLoteRpsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultarSituacaoLoteRps }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.nfse.ws.webservicenfse.edza.com.br/", name = "ConsultarSituacaoLoteRps")
    public JAXBElement<ConsultarSituacaoLoteRps> createConsultarSituacaoLoteRps(ConsultarSituacaoLoteRps value) {
        return new JAXBElement<ConsultarSituacaoLoteRps>(_ConsultarSituacaoLoteRps_QNAME, ConsultarSituacaoLoteRps.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultarNfseResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.nfse.ws.webservicenfse.edza.com.br/", name = "ConsultarNfseResponse")
    public JAXBElement<ConsultarNfseResponse> createConsultarNfseResponse(ConsultarNfseResponse value) {
        return new JAXBElement<ConsultarNfseResponse>(_ConsultarNfseResponse_QNAME, ConsultarNfseResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CancelarNfse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.nfse.ws.webservicenfse.edza.com.br/", name = "CancelarNfse")
    public JAXBElement<CancelarNfse> createCancelarNfse(CancelarNfse value) {
        return new JAXBElement<CancelarNfse>(_CancelarNfse_QNAME, CancelarNfse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultarLoteRps }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.nfse.ws.webservicenfse.edza.com.br/", name = "ConsultarLoteRps")
    public JAXBElement<ConsultarLoteRps> createConsultarLoteRps(ConsultarLoteRps value) {
        return new JAXBElement<ConsultarLoteRps>(_ConsultarLoteRps_QNAME, ConsultarLoteRps.class, null, value);
    }

}
