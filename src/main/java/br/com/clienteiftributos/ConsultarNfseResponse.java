
package br.com.clienteiftributos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ConsultarNfseResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ConsultarNfseResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ConsultarNfseResponse" type="{http://endpoint.nfse.ws.webservicenfse.edza.com.br/}output" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConsultarNfseResponse", propOrder = {
    "consultarNfseResponse"
})
public class ConsultarNfseResponse {

    @XmlElement(name = "ConsultarNfseResponse")
    protected Output consultarNfseResponse;

    /**
     * Gets the value of the consultarNfseResponse property.
     * 
     * @return
     *     possible object is
     *     {@link Output }
     *     
     */
    public Output getConsultarNfseResponse() {
        return consultarNfseResponse;
    }

    /**
     * Sets the value of the consultarNfseResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link Output }
     *     
     */
    public void setConsultarNfseResponse(Output value) {
        this.consultarNfseResponse = value;
    }

}
