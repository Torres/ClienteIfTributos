
package br.com.clienteiftributos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CancelarNfse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CancelarNfse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CancelarNfseRequest" type="{http://endpoint.nfse.ws.webservicenfse.edza.com.br/}input" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CancelarNfse", propOrder = {
    "cancelarNfseRequest"
})
public class CancelarNfse {

    @XmlElement(name = "CancelarNfseRequest")
    protected Input cancelarNfseRequest;

    /**
     * Gets the value of the cancelarNfseRequest property.
     * 
     * @return
     *     possible object is
     *     {@link Input }
     *     
     */
    public Input getCancelarNfseRequest() {
        return cancelarNfseRequest;
    }

    /**
     * Sets the value of the cancelarNfseRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link Input }
     *     
     */
    public void setCancelarNfseRequest(Input value) {
        this.cancelarNfseRequest = value;
    }

}
