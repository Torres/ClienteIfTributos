
package br.com.clienteiftributos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ConsultarLoteRpsResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ConsultarLoteRpsResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ConsultarLoteRpsResponse" type="{http://endpoint.nfse.ws.webservicenfse.edza.com.br/}output" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConsultarLoteRpsResponse", propOrder = {
    "consultarLoteRpsResponse"
})
public class ConsultarLoteRpsResponse {

    @XmlElement(name = "ConsultarLoteRpsResponse")
    protected Output consultarLoteRpsResponse;

    /**
     * Gets the value of the consultarLoteRpsResponse property.
     * 
     * @return
     *     possible object is
     *     {@link Output }
     *     
     */
    public Output getConsultarLoteRpsResponse() {
        return consultarLoteRpsResponse;
    }

    /**
     * Sets the value of the consultarLoteRpsResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link Output }
     *     
     */
    public void setConsultarLoteRpsResponse(Output value) {
        this.consultarLoteRpsResponse = value;
    }

}
